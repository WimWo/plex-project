package be.ordina;

import static org.junit.Assert.assertTrue;
import static org.assertj.core.api.Assertions.*;

import be.ordina.pages.HomePage;
import be.ordina.pages.LoginModel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import java.io.IOException;

public class AppTest {
    private WebDriver driver;
    private String email = "testingplex54321@mailinator.com"; //TODO make it random?
    private String password = "ditiseenwachtwoord"; //TODO make it random?

    private LoginModel loginModel;
    private HomePage homePage;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\Wiwl\\Desktop\\TA-Session2\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://app.plex.tv/");
        //driver.manage().window().maximize();
        loginModel = new LoginModel(driver);
        homePage = new HomePage(driver);

    }

    @After
    public void tearDown() {
        driver.close();
    }

    /*@Test
    public void wrongCredentialsLogin() {

    }*/

    /*@Test
    public void createNewAccount() throws InterruptedException {
        driver.get("https://www.plex.tv/");
        //LoginModel loginModel = new LoginModel(driver);
        //HomePage homePage = new HomePage(driver);
        loginModel.acceptCookies();
        loginModel.register(email, password);
        homePage.validateHomepage();

        //String capturedEmailAddress = homePage.getLoggedInUser();
        //assertTrue(capturedEmailAddress.contains(email));
        assertThat(homePage.getLoggedInUser()).contains(email);

        Thread.sleep(10000);
    }*/

    @Test
    public void startStream() throws InterruptedException, IOException {
        //LoginModel loginModel = new LoginModel(driver);
        //HomePage homePage = new HomePage(driver);

        //loginModel.acceptCookies();
        //loginModel.login(email, password);

        homePage.validateHomepage();

        System.out.println("voor random stream");
        homePage.startRandomStream();

        System.out.println("na random stream");
        //Thread.sleep(10000);
    }

    @Test
    public void searchMovie() throws IOException, InterruptedException {
        //LoginModel loginModel = new LoginModel(driver);
        //HomePage homePage = new HomePage(driver);

        //loginModel.acceptCookies();
        //loginModel.login(email, password);

        homePage.searchMovie();
        //Thread.sleep(10000);
    }

    @Test
    public void addMovieToWatchlist() throws InterruptedException {
        driver.get("https://www.plex.tv/");
        //LoginModel loginModel = new LoginModel(driver);
        //HomePage homePage = new HomePage(driver);

        loginModel.acceptCookies();
        loginModel.login(email, password);

        homePage.addMovieToWatchlist();
        Thread.sleep(10000);
    }
}
