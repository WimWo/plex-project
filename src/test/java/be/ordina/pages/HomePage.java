package be.ordina.pages;

import be.ordina.utils.SeleniumUtils;
import io.qameta.allure.Allure;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;
import static org.assertj.core.api.Assertions.*;

public class HomePage {

    private final WebDriver driver;
    private WebDriverWait wait;
    private Actions action;

    private By navBar = By.className("nav-bar");
    private By btnAccount = By.id("id-10");

    private By textHomepage = By.xpath("//*[@id='content']/div/div/div/div/div/span[contains(text(),'Home')]");

    private By btnDropdownAccount = By.xpath("//*[@id='id-13']//a[contains(@href,'account')]");
    private By textAcccountEmailAdres = By.xpath("//*[@id='content']//label[contains(text(),'Email Address')]/..");

    private By thumbnailRecentMovie = By.xpath("//div[@data-qa-id='hubTitle']/a[contains(text(), 'Recently Added')]/../../..//div/div/div/div/div/div"); //className("MetadataPosterListItem-card-15XOx1");
    //TODO change bovenstaande gelijkaardig aan: //div[@data-qa-id='hubTitle']/a[contains(text(),'Most Popular')]/../../..//a[contains(@href,'hub.movies.popular')]
    private By thumbnailPopularMovie = By.xpath("//div[@data-qa-id='hubTitle']/a[contains(text(),'Most Popular')]/../../..//a[contains(@href,'hub.movies.popular')]");

    private By videoPlayer = By.cssSelector("div.application.show-video-player");

    private By btnConsentAgree = By.xpath("//div[contains(@class,'Modal-modalContainer')]//span[text()='I Agree']");     //("//button[@data-uid='id-270']");

    private By inputQuickSearch = By.xpath("//div[@class='nav-bar']//input[@data-qa-id='quickSearchInput']");
    private By quickSearchResults = By.xpath("//div[@class='nav-bar']//a[@data-qa-id='quickSearchItemLink' and contains(@title,'Red')]"); //multiple results!
    private By quickSearchResult = By.xpath("//div[@class='nav-bar']//a[@data-qa-id='quickSearchItemLink' and contains(@title,'Red Is the Color of')]"); //one result to click on!

    private By quickSearchContainer = By.xpath("//div[@class='nav-bar']//input[@data-qa-id='quickSearchInput']/../..");

    private By prePlayView = By.xpath("//div[@id='content']//div[@data-qa-id='preplay-mainTitle']");

    private By btnAddToWatchlist = By.xpath("//div[@id='content']//button[@data-qa-id='preplay-addToWatchlist']");

    private By sidebarMoviesAndShows = By.xpath("//div[@id='content']//div[@data-qa-id='sidebarSource']/a[@title='Movies & Shows']");
    private By tabWatchlist = By.xpath("//div[@id='content']//a[@data-qa-id='tabButton' and text()='Watchlist']");
    private By nrOnWatchlist = By.xpath("//div[@id='content']//div[contains(@class, 'PageHeader-pageHeader')]//span[@data-qa-id='badgeElement']");
    private By thumbnailInWatchlist = By.xpath("//div[@id='content']//div[@data-qa-id='cellItem']");
    private By btnDeleteFromWatchlist = By.xpath("//button[@data-qa-id='dropdownItem' and contains(text(),'Delete')]");

    
    public HomePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 20);
        this.action = new Actions(driver);
    }

    public void validateHomepage() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(textHomepage));

        assertThat(driver.findElement(textHomepage).getText()).isEqualTo("Home");
        //assertThat(driver.findElement(textHomepage).getText()).is;
        /*if(driver.findElement(textHomepage).getText().equals("Home")) {
            return true;
        }
        return false;*/
    }


    public String getLoggedInUser() throws InterruptedException {
        //System.out.println("TEEEEEEEEEEEEEEEST************");
        //Thread.sleep(10000); //TODO REMOVE
        //wait.until(ExpectedConditions.visibilityOfElementLocated(navBar));
        wait.until(ExpectedConditions.visibilityOfElementLocated(btnAccount));
        driver.findElement(btnAccount).click();
        driver.findElement(btnDropdownAccount).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(textAcccountEmailAdres));
        //System.out.println(driver.findElement(textAcccountEmailAdres).getText());
        return driver.findElement(textAcccountEmailAdres).getText();
    }

    public void startRandomStream() throws IOException {
        WebElement we = wait.until(ExpectedConditions.visibilityOfElementLocated(thumbnailRecentMovie));
        action.moveToElement(we).perform();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='content']//div/button[@aria-label='Play']")));
        driver.findElement(By.xpath("//*[@id='content']//div/button[@aria-label='Play' or @aria-label='Resume']")).click(); ////*[@id="content"]/div/div/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/div[1]/div/button[1]

        /*try {
            driver.findElement(btnConsentAgree).isDisplayed();
            System.out.println("Consent is displayed");
            driver.findElement(btnConsentAgree).click();
        } catch (Exception e) {
            System.out.println("Consent is not displayed");
        }*/
        driver.findElement(btnConsentAgree).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(videoPlayer));
        driver.findElement(videoPlayer);
        //TODO add assertion
        SeleniumUtils.screenshot(driver,"video_player");
        //System.out.println("********SUCCESS*********"); //action.moveToElement(we).moveToElement(driver.findElement(By.xpath("..."))).click.build();
    }

    public void searchMovie() throws IOException {
        wait.until(ExpectedConditions.visibilityOfElementLocated(inputQuickSearch));
        String search = "red";
        driver.findElement(inputQuickSearch).sendKeys(search);

        // ACCEPTANCE CRITERIA 1: show suggestions related to search
        wait.until(ExpectedConditions.visibilityOfElementLocated(quickSearchResults));
        //System.out.println(driver.findElement(quickSearchResults).getAttribute("title")); //possibility to get all elements with method 'findElementS' and print them all
        List resultElements = driver.findElements(quickSearchResults).stream().map(m -> m.getAttribute("title")).collect(Collectors.toList());
        //Allure.addAttachment("Lijst van resultaten:", driver.findElements(quickSearchResults));
        //Allure.addAttachment("Lijst van resultaten:", resultElements);
        System.out.println("---------------teeeestt----------");
        //System.out.println(Arrays.toString(resultElements.toArray()));
        //System.out.println(Arrays.toString(list.toArray()));
        Allure.addAttachment("test resultaten lijst:", String.valueOf(resultElements));
        SeleniumUtils.screenshot(driver,"search");

        driver.findElement(quickSearchResult).click();

        // ACCEPTANCE CRITERIA 2: click on suggestion opens detail page
        wait.until(ExpectedConditions.visibilityOfElementLocated(prePlayView));

        SeleniumUtils.screenshot(driver,"preplay_page");
    }

    public void addMovieToWatchlist() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(sidebarMoviesAndShows));
        driver.findElement(sidebarMoviesAndShows).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(tabWatchlist));
        driver.findElement(tabWatchlist).click();

        while (Integer.parseInt(driver.findElement(nrOnWatchlist).getText()) > 1) {
            WebElement we = wait.until(ExpectedConditions.visibilityOfElementLocated(thumbnailInWatchlist));
            action.moveToElement(we).perform();

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='content']//div[@data-qa-id='cellItem']//button[@data-qa-id='metadataPosterMoreButton']")));
            driver.findElement(By.xpath("//div[@id='content']//div[@data-qa-id='cellItem']//button[@data-qa-id='metadataPosterMoreButton']")).click();

        }

        wait.until(ExpectedConditions.visibilityOfElementLocated(thumbnailPopularMovie));
        driver.findElement(thumbnailPopularMovie).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(btnAddToWatchlist));
        driver.findElement(btnAddToWatchlist).click(); //TODO add conditional in case it is already added to watchlist

        assertTrue(driver.findElement(btnAddToWatchlist).isDisplayed());
    }

    /*
    public void clickSignIn() {
        driver.findElement(btnAccount).click();
        driver.findElement(btnAccountSignIn).click();
    }

    public String getSuccessMessage() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(textLoginConfirmation));
        String message = driver.findElement(textLoginConfirmation).getText();
        return message;
    }*/

}
