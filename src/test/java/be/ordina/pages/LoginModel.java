package be.ordina.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginModel {

    private final WebDriver driver;
    private WebDriverWait wait;

    private By btnAcceptCookies = By.className("cookie-consent-accept");
    private By btnSignUp = By.className("signup");
    private By btnSignIn = By.className("signin");

    private By inputEmail = By.id("email"); //xpath("//*[@id='email']");
    private By inputPassword = By.id("password"); //xpath("//*[@id='password']");

    private By btnSignSubmit = By.xpath("//*[@id='plex']/*//form/button");
    private By btnLaunch = By.className("launch");


    /*public LoginModel(WebDriver driver) {
        this.driver = driver;
    }*/

    public LoginModel(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 20);
    }

    public void acceptCookies() {
        driver.findElement(btnAcceptCookies).click();
    }

    public void register(String email, String password) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(btnSignUp));
        driver.findElement(btnSignUp).click();

        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("fedauth-iFrame"));
        //driver.switchTo().frame("fedauth-iFrame");

        wait.until(ExpectedConditions.visibilityOfElementLocated(inputEmail));
        driver.findElement(inputEmail).sendKeys(email);
        driver.findElement(inputPassword).sendKeys(password);
        driver.findElement(btnSignSubmit).submit();
    }

    public void login(String email, String password) {
        driver.findElement(btnSignIn).click();
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("fedauth-iFrame"));
        wait.until(ExpectedConditions.visibilityOfElementLocated(inputEmail));
        driver.findElement(inputEmail).sendKeys(email);
        driver.findElement(inputPassword).sendKeys(password);
        driver.findElement(btnSignSubmit).submit();
        wait.until(ExpectedConditions.visibilityOfElementLocated(btnLaunch));
        driver.findElement(btnLaunch).click();
    }

    /*
    public void login(String username, String password) {
        driver.findElement(tbUsername).sendKeys(username);
        WebElement element = driver.findElement(tbPassword); //We need to do two actions so we declare it as element
        element.sendKeys(password);
        element.submit();
    }*/

}
