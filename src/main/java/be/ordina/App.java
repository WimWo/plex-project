package be.ordina;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        System.setProperty("webdriver.chrome.driver","C:\\Users\\Wiwl\\Desktop\\TA-Session2\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.plex.tv/");
        WebElement accountBtn = driver.findElement(By.id("account-menu"));
        accountBtn.click();
        WebElement signInBtn = driver.findElement(By.id("login"));
        signInBtn.click();
        WebElement usernameInput = driver.findElement(By.id("username"));
        usernameInput.sendKeys("username");
        WebElement passwordInput = driver.findElement(By.id("password"));
        passwordInput.sendKeys("password");
        //passwordInput.submit();
        WebElement formBtn = driver.findElement(By.xpath("//button[@type='submit']"));
        formBtn.click();

    }
}
